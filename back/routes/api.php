<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', 'PassportController@login');
Route::post('/register', 'PassportController@register');

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//librarian
Route::post('/librarian/users/approve', 'LibrarianController@usersPendingApprove');
Route::post('/librarian/users/reject', 'LibrarianController@usersPendingReject');

//books
Route::post('/librarian/books', 'LibrarianController@saveBooks')->name('saveBooks');
Route::get('/librarian/book/{id}/edit', 'LibrarianController@editBooksAPI');
Route::post('/librarian/book/{id}/save', 'LibrarianController@saveEditBooksAPI');
Route::post('/librarian/book/search', 'LibrarianController@searchBooksAPI');
Route::get('/librarian/book/{id}/delete', 'LibrarianController@deleteBooksAPI');
Route::get('/librarian/book/list/all', 'LibrarianController@listBooksAllAPI');
Route::get('/librarian/book/list/', 'LibrarianController@listBooksAPI');
Route::get('/librarian/country/list/', 'LibrarianController@listCountryAPI');