<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logged/{token}/{id}', 'PassportController@logged');

//Librarian
Route::get('/librarian/users/pending', 'LibrarianController@usersPending')->name('usersPending');
Route::get('/librarian/books/add', 'LibrarianController@addBooks')->name('addBooks');
Route::get('/librarian/books/list', 'LibrarianController@listBooks')->name('listBooks');
Route::get('/librarian/book/{id}/edit', 'LibrarianController@editBooks');



