<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Countrylist;
use App\User;
use Illuminate\Http\Request;

class LibrarianController extends Controller
{
    public function usersPending()
    {
        $pendings = User::where('status', 0)->where('country', '!=', 'Sri Lanka')->where('role', 'user')->get();

        return view('librarian.users_pending')
            ->with('pendings', $pendings);
    }

    public function usersPendingApprove(Request $request)
    {
        $user = User::where('id', $request->id)->update(['status' => 1]);
        return response()->json('success', 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    public function usersPendingReject(Request $request)
    {
        $user = User::where('id', $request->id)->update(['status' => -1]);
        return response()->json('success', 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

//   books

    public function addBooks()
    {
        return view('librarian.add-books');
    }

    public function saveBooks(Request $request)
    {
        $book = new Book($request->all());
        $res = $book->save();

        return response()->json(['status' => $res], 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    public function saveEditBooksAPI(Request $request)
    {

        $book = Book::where('id', $request->id)->first();
        $book->title = $request->title;
        $book->author = $request->author;
        $book->year = $request->year;
        $book->category = $request->category;
        $book->type = $request->type;
        $book->isbn = $request->isbn;
        $book->publisher = $request->publisher;
        $book->price = $request->price;
        $res = $book->save();

        return response()->json(['status' => $res], 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    public function listBooks()
    {
        $books = Book::get();
        return view('librarian.list-books')
            ->with('books', $books);
    }

    public function listBooksAllAPI()
    {
        $books = Book::get()->toJson();
        return $books;
    }

    public function listBooksAPI()
    {
        $books = Book::where('status', 1)->get()->toJson();
        return $books;
    }

    public function editBooksAPI($id)
    {
        $books = Book::where('id', $id)->first()->toJson();
        return $books;
    }
    public function editBooks($id)
    {
        $books = Book::where('id', $id)->first();
        return view('librarian.edit-books')
            ->with('books',$books);
    }

    public function deleteBooksAPI($id)
    {
        $books = Book::where('id', $id)->delete();
        return response()->json(['status' => $books], 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    public function searchBooksAPI(Request $request)
    {
        $books = Book::where('status', 1);
        if ($request->year) {
            $books->where('year', $request->year);
        }
        if ($request->author) {
            $books->where('author', $request->author);
        }
        if ($request->category) {
            $books->where('category', $request->category);
        }
        if ($request->type) {
            $books->where('type', $request->type);
        }
        if ($request->isbn) {
            $books->where('isbn', $request->isbn);
        }
        if ($request->type) {
            $books->where('type', $request->type);
        }
        $res = $books->get();
        return response()->json($res, 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    public function listCountryAPI(){
        $res = Countrylist::get()->toJson();
        return $res;
    }
}
