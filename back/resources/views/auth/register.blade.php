@extends('layouts.app')

@section('content')
    <body id="LoginForm">
    <div class="container">

        <h1 class="form-heading">Register Form</h1>
        <div class="login-form">
            <div class="main-div">
                <div class="panel">
                    <h2>Register Now</h2>
                    <p>Please enter your your details</p>
                </div>
                <form class="form-horizontal" id="register_form">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="name" type="text" class="form-control" name="name" placeholder="Your Name"
                           value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email"
                           placeholder="Your Email"
                           value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group ">
                    <select name="country" id="country" class="form-control">
                        <option value="">Select Country</option>
                        @foreach($countries as $country)
                            <option value="{{$country->nicename}}">{{$country->name}}</option>
                        @endforeach

                    </select>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password"
                           placeholder="Password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control"
                           placeholder="password confirmation"
                           name="password_confirmation" required>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="button" class="btn btn-primary" id="register_btn">
                            Register
                        </button>
                    </div>
                </div>
                </form>
            </div>
            {{--<p class="botto-text"> Designed by Nethuli Nsbm Plymouth</p>--}}
        </div>
    </div>
    </div>


    </body>

@endsection

@section('extra-js')
    <script>
        $('#register_btn').click(function (e) {
            e.preventDefault();
            var errors = '';
            if ($('#name').val() == '') {
                errors = errors + 'Fill the name \n';
            }
            if ($('#email').val() == '') {
                errors = errors + 'Fill the Email\n';
            }
            if ($('#country').val() == '') {
                errors = errors + 'Fill the country\n';
            }
            if ($('#password').val() == '') {
                errors = errors + 'Fill the password\n';
            }
            if ($('#password-confirm').val() == '') {
                errors = errors + 'Fill the password confirm\n';
            }
            if ($('#password').val() != $('#password-confirm').val()) {
                errors = errors + 'Passwords not matched\n';
            }

            if (errors == '') {
                var data = $('#register_form').serialize();
                $.ajax({
                    url: "/api/register",
                    type: 'POST',
                    data: data,
                    success: function (res) {
                        if (res.status == 'exsits') {
                            alert('Provided Email Already exists');
                        } else if (res.status == 'password_not_matched') {
                            alert('Provided Password not matched');
                        }
                        if (res.status == 'success') {
                            window.location = '/logged/' + res.token + '/' + res.user.id;
                        } else {

                        }
                    }
                });
            } else {
                alert(errors);
            }

        });
    </script>
@endsection