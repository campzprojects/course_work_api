@extends('layouts.app')
@section('extra-css')

@endsection
@section('content')
    <body id="LoginForm">
    <div class="container">

        <h1 class="form-heading">login Form</h1>
        <div class="login-form">
            <div class="main-div">
                <div class="panel">
                    <h2>Admin Login</h2>
                    <p>Please enter your email and password</p>
                </div>
                <form class="form-horizontal" id="login_form">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    </div>
                    <div class="form-group">
                        <input id="email" type="email" class="form-control" name="email" placeholder="Email"
                               value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group">

                        <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif

                    </div>
                    <div class="forgot">
                        Don't have account? <a class="btn btn-link" href="{{ route('register') }}">Register</a>
                    </div>
                    <button type="submit" class="btn btn-primary" id="login_btn">Login</button>

                </form>
            </div>
            <p class="botto-text"> Designed by Nethuli Nsbm Plymouth</p>
        </div>
    </div>
    </div>


    </body>

@endsection

@section('extra-js')
    <script>
        $('#login_btn').click(function (e) {
            e.preventDefault();
            var data = $('#login_form').serialize();
            $.ajax({
                url: "/api/login",
                type: 'POST',
                data: data,
                success: function (res) {
                   if(res.status =='success'){
                       window.location = '/logged/'+res.token+'/'+ res.user.id;
                   }else{
                       alert('Provided Credentials are invalid');
                   }
                }
            });
        });
    </script>
@endsection