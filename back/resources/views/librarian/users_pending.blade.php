@extends('layouts.admin_layout')

@section('content')
    <section class="content-header">
        <h1>
            Pending Users
            <small>{{\Illuminate\Support\Facades\Auth::user()->role}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pending Users</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="box box-success">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-user-circle"></i>
                            <h3 class="box-title">Search Customer Here</h3>

                        </div>
                        <div class="box-body">
                            <div class="col-md-12">
                                <table id="example" class="display" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th width="10px">#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Country</th>
                                        <th>Status</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $x=1; @endphp
                                    @foreach($pendings as $user)
                                        <tr>
                                            <td>{{$x}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->country}}</td>
                                            <td>{{$user->status}}</td>
                                            <td>{{$user->role}}</td>
                                            <td>
                                                <button class="btn btn-info btn-sm" onclick="approve({{$user->id}})">Approve</button>&nbsp;
                                                <button class="btn btn-danger btn-sm" onclick="reject({{$user->id}})">Reject</button>&nbsp;
                                            </td>
                                        </tr>
                                        @php $x++; @endphp

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });

        function approve(id) {
            var x = confirm('Are you sure?');
            if (x){
                $.ajax({
                    url: "/api/librarian/users/approve",
                    type: 'POST',
                    data: {'id':id},
                    success: function (res) {
                        if(res =='success'){
                            alert('Updated Successfully');
                            setTimeout(function () {
                                location.reload();
                            },1000);
                        }else{
                            alert('Something went wrong');
                        }
                    }
                });
            }

        }

        function reject(id) {
            var x = confirm('Are you sure?');
            if (x){
                $.ajax({
                    url: "/api/librarian/users/reject",
                    type: 'POST',
                    data: {'id':id},
                    success: function (res) {
                        if(res =='success'){
                            alert('Updated Successfully');
                            setTimeout(function () {
                                location.reload();
                            },1000);
                        }else{
                            alert('Something went wrong');
                        }
                    }
                });
            }

        }
    </script>
@endsection
