@extends('layouts.admin_layout')

@section('content')
    <section class="content-header">
        <h1>
            Dashboard
            <small>{{\Illuminate\Support\Facades\Auth::user()->role}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="col-md-offset-3 col-md-6">
                        <h3>Edit Book</h3>
                        <form action="" id="add-book-form">
                            <input type="hidden" name="id" value="{{$books->id}}">
                            <div class="form-group">
                                <input type="text" name="title" placeholder="Book Title" class="form-control" required
                                       id="title" value="{{$books->title}}">
                            </div>
                            <div class="form-group">
                                <input type="text" name="author" placeholder="Book author" class="form-control" required
                                       id="author" value="{{$books->author}}">
                            </div>
                            <div class="form-group">
                                <select name="year" id="year" class="form-control" required>
                                    <option value="">Select Book Year</option>
                                    @for($x=2019 ; $x>=1950; $x--)
                                        <option value="{{$x}}" {{($books->year == $x) ? 'selected' :''}}>{{$x}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="category" id="category" class="form-control" required>
                                    <option value="">Select Book Category</option>
                                    <option value="magazine" {{($books->category == 'magazine') ? 'selected' :''}}>Magazine</option>
                                    <option value="book" {{($books->category == 'book') ? 'selected' :''}}>Book</option>
                                    <option value="news-paper" {{($books->category == 'news-paper') ? 'selected' :''}}>News Paper</option>
                                    <option value="government-publications" {{($books->category == 'government-publications') ? 'selected' :''}}>Government Publications</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <input type="radio" name="type" value="public" {{($books->type == 'public') ? 'checked' :''}}> Public
                                <input type="radio" name="type" value="rare" {{($books->type == 'rare') ? 'checked' :''}}> Rare
                            </div>
                            <div class="form-group">
                                <input type="text" name="isbn" id="isbn" placeholder="Book ISBN" class="form-control"
                                       required value="{{$books->isbn}}">
                            </div>
                            <div class="form-group">
                                <input type="text" name="publisher" id="publisher" placeholder="Book Publisher"
                                       class="form-control" required value="{{$books->publisher}}">
                            </div>
                            <div class="form-group">
                                <input type="number" name="price" id="price" placeholder="Book Price"
                                       class="form-control" required value="{{$books->price}}">
                            </div>
                            <div class="form-group">
                                <input type="button" class="btn btn-success" value="Update Book" id="add-book-btn">
                            </div>
                        </form>

                    </div>
                    <div class="clearfix"></div>
                </div> <!-- card.// -->
            </div>
        </div>

    </section>


@endsection

@section('extra-js')
    <script>
        var url = "/api/librarian/book/{{$books->id}}/save";
        $('#add-book-btn').click(function (e) {
            e.preventDefault();
            var error = '';

            if ($('#title').val() == '') {
                error = error + 'Enter Book title\n';
            }
            if ($('#author').val() == '') {
                error = error + 'Enter Book author\n';
            }
            if ($('#year').val() == '') {
                error = error + 'Enter Book year\n';
            }
            if ($('#category').val() == '') {
                error = error + 'Enter Book category\n';
            }
            if ($('#isbn').val() == '') {
                error = error + 'Enter Book isbn number\n';
            }
            if ($('#publisher').val() == '') {
                error = error + 'Enter Book publisher\n';
            }
            if ($('#price').val() == '') {
                error = error + 'Enter Book price\n';
            }

            if (error == '') {
                var data = $('#add-book-form').serialize();
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    success: function (res) {
                        if (res.status) {
                            alert('Book updated successfully!');
                            setTimeout(function (e) {
                                location.reload();
                            },1500);

                        } else {
                            alert('Error! Book not updated');

                        }
                    }
                });
            }else{
                alert(error);
            }

        });
    </script>
@endsection
