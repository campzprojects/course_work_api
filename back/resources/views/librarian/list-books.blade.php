@extends('layouts.admin_layout')

@section('content')
    <section class="content-header">
        <h1>
            Dashboard
            <small>{{\Illuminate\Support\Facades\Auth::user()->role}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <table class="table table-hover shopping-cart-wrap" id="example">
                        <thead class="text-muted">
                        <tr class="heads">
                            <th scope="col">Item</th>
                            <th scope="col" width="100">Year</th>
                            <th scope="col" width="120">Price</th>
                            <th scope="col" width="120">Category</th>
                            <th scope="col" width="120">IBSN Number</th>
                            <th scope="col" width="140" class="text-right">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($books as $book)
                            <tr>
                                <td>
                                    <figure class="media">
                                        <div class="img-wrap"><img src="{{ asset('img/notebook.png') }}"
                                                                   class="img-thumbnail img-sm"></div>
                                        <figcaption class="media-body">
                                            <h6 class="title text-truncate">{{$book->title}}</h6>
                                            <dl class="param param-inline small">
                                                <dt>Author:</dt>
                                                <dd>{{$book->author}}</dd>
                                                <br>
                                                <dt>Publisher</dt>
                                                <dd>{{$book->publisher}}</dd>
                                                <br>
                                                <dt>Status</dt>
                                                <dd>{{($book->status)? 'Active':'Deactive'}}</dd>
                                                <br>
                                                <br>
                                                @if($book->type == 'public')
                                                    Type <a class="public" href="#">Public</a>
                                                @else
                                                    Type <dt><a class="rare" href="#">Rare</a></dt>
                                                @endif
                                            </dl>
                                        </figcaption>
                                    </figure>
                                </td>
                                <td>
                                    <p>
                                        <span class="cat">{{$book->year}}</span>
                                    </p>
                                </td>
                                <td>
                                    <p>
                                        <span class="cat">LKR {{$book->price}}</span>
                                    </p>
                                </td>
                                <td>
                                    <p>
                                        <span class="cat">{{$book->category}}</span>
                                    </p>
                                </td>
                                <td>
                                    <p>
                                        <span class="ibsn">{{$book->isbn}}</span>
                                    </p>
                                </td>
                                <td class="text-right">
                                    {{--<a href="" class="ac btn btn-default btn-sm">Deactivate</a>--}}
                                    <a href="{{asset('')}}librarian/book/{{$book->id}}/edit" class="ac btn btn-warning btn-sm">Edit</a>
                                    <button class="ac btn btn-danger btn-sm" onclick="deleteBook({{$book->id}})">Delete</button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div> <!-- card.// -->
            </div>
        </div>

    </section>


@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });

        var url = "{{route('saveBooks')}}";
        $('#add-book-btn').click(function (e) {
            e.preventDefault();
            var error = '';

            if ($('#title').val() == '') {
                error = error + 'Enter Book title\n';
            }
            if ($('#author').val() == '') {
                error = error + 'Enter Book author\n';
            }
            if ($('#year').val() == '') {
                error = error + 'Enter Book year\n';
            }
            if ($('#category').val() == '') {
                error = error + 'Enter Book category\n';
            }
            if ($('#isbn').val() == '') {
                error = error + 'Enter Book isbn number\n';
            }
            if ($('#publisher').val() == '') {
                error = error + 'Enter Book publisher\n';
            }
            if ($('#price').val() == '') {
                error = error + 'Enter Book price\n';
            }

            if (error == '') {
                var data = $('#add-book-form').serialize();
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    success: function (res) {
                        if (res.status) {
                            alert('Book added successfully!');
                            setTimeout(function (e) {
                                location.reload();
                            }, 1500);

                        } else {
                            alert('Error! Book not added');

                        }
                    }
                });
            } else {
                alert(error);
            }

        });

//        delete
        function deleteBook(id) {
            var url = '/api/librarian/book/'+id+'/delete';
            $.get(url, {})
                .done(function(response) {
                    alert("Deleted successfully!");
                    setTimeout(function (e) {
                        location.reload();
                    },1000);
                });
        }
    </script>
@endsection
