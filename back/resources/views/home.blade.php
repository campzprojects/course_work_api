@extends('layouts.admin_layout')

@section('content')
    <section class="content-header">
        <h1>
            Dashboard
            <small>{{\Illuminate\Support\Facades\Auth::user()->role}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Books</span>
                        <span class="info-box-number">1,410</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Magezines</span>
                        <span class="info-box-number">410</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">News Papers</span>
                        <span class="info-box-number">13,648</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Users</span>
                        <span class="info-box-number">93,139</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>

       


        {{--<div class="row">--}}

            {{--<div class="col-md-12">--}}

                {{--<div class="card">--}}
                    {{--<table class="table table-hover shopping-cart-wrap">--}}
                        {{--<thead class="text-muted">--}}
                        {{--<tr class="heads">--}}
                            {{--<th scope="col">Item Name</th>--}}
                            {{--<th scope="col" width="120">Category</th>--}}
                            {{--<th scope="col" width="120">IBSN Number</th>--}}
                            {{--<th scope="col" width="200" class="text-right">Actions</th>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                            {{--<td>--}}
                                {{--<figure class="media">--}}
                                    {{--<div class="img-wrap"><img src="{{ asset('img/notebook.png') }}"--}}
                                                               {{--class="img-thumbnail img-sm"></div>--}}
                                    {{--<figcaption class="media-body">--}}
                                        {{--<h6 class="title text-truncate">Ready to player one </h6>--}}
                                        {{--<dl class="param param-inline small">--}}
                                            {{--<dt>Author:</dt>--}}
                                            {{--<dd>Tim Mart</dd>--}}
                                        {{--</dl>--}}
                                        {{--<dl class="param param-inline small">--}}
                                            {{--<dt>Item Category:</dt>--}}
                                            {{--<dd>Novels</dd>--}}
                                            {{--<br>--}}
                                            {{--<br>--}}
                                            {{--<dt><a class="rare" href="#">Rare</a></dt>--}}
                                            {{--<a class="public" href="#">public</a>--}}
                                        {{--</dl>--}}
                                    {{--</figcaption>--}}
                                {{--</figure>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<p>--}}
                                    {{--<span class="cat">Novels</span>--}}
                                {{--</p>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<p>--}}
                                    {{--<span class="ibsn">22220220</span>--}}
                                {{--</p>--}}
                            {{--</td>--}}
                            {{--<td class="text-right">--}}
                                {{--<a href="" class="ac btn btn-danger"> <i class="fa fa-eye"></i> View</a>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>--}}
                                {{--<figure class="media">--}}
                                    {{--<div class="img-wrap"><img src="{{ asset('img/notebook.png') }}"--}}
                                                               {{--class="img-thumbnail img-sm"></div>--}}
                                    {{--<figcaption class="media-body">--}}
                                        {{--<h6 class="title text-truncate">Ready to player one </h6>--}}
                                        {{--<dl class="param param-inline small">--}}
                                            {{--<dt>Author:</dt>--}}
                                            {{--<dd>Tim Mart</dd>--}}
                                        {{--</dl>--}}
                                        {{--<dl class="param param-inline small">--}}
                                            {{--<dt>Item Category:</dt>--}}
                                            {{--<dd>Novels</dd>--}}
                                            {{--<br>--}}
                                            {{--<br>--}}
                                            {{--<dt><a class="rare" href="#">Rare</a></dt>--}}
                                            {{--<a class="public" href="#">public</a>--}}
                                        {{--</dl>--}}
                                    {{--</figcaption>--}}
                                {{--</figure>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<p>--}}
                                    {{--<span class="cat">Novels</span>--}}
                                {{--</p>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<p>--}}
                                    {{--<span class="ibsn">22220220</span>--}}
                                {{--</p>--}}
                            {{--</td>--}}
                            {{--<td class="text-right">--}}
                                {{--<a href="" class="ac btn btn-danger"> <i class="fa fa-eye"></i> View</a>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>--}}
                                {{--<figure class="media">--}}
                                    {{--<div class="img-wrap"><img src="{{ asset('img/notebook.png') }}"--}}
                                                               {{--class="img-thumbnail img-sm"></div>--}}
                                    {{--<figcaption class="media-body">--}}
                                        {{--<h6 class="title text-truncate">Ready to player one </h6>--}}
                                        {{--<dl class="param param-inline small">--}}
                                            {{--<dt>Author:</dt>--}}
                                            {{--<dd>Tim Mart</dd>--}}
                                        {{--</dl>--}}
                                        {{--<dl class="param param-inline small">--}}
                                            {{--<dt>Item Category:</dt>--}}
                                            {{--<dd>Novels</dd>--}}
                                            {{--<br>--}}
                                            {{--<br>--}}
                                            {{--<dt><a class="rare" href="#">Rare</a></dt>--}}
                                            {{--<a class="public" href="#">public</a>--}}
                                        {{--</dl>--}}
                                    {{--</figcaption>--}}
                                {{--</figure>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<p>--}}
                                    {{--<span class="cat">Novels</span>--}}
                                {{--</p>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<p>--}}
                                    {{--<span class="ibsn">22220220</span>--}}
                                {{--</p>--}}
                            {{--</td>--}}
                            {{--<td class="text-right">--}}
                                {{--<a href="" class="ac btn btn-danger"> <i class="fa fa-eye"></i> View</a>--}}
                            {{--</td>--}}
                        {{--</tr>--}}

                        {{--</tbody>--}}
                    {{--</table>--}}
                {{--</div> <!-- card.// -->--}}
            {{--</div>--}}
        {{--</div>--}}

    </section>


@endsection
