
<header class="main-header">
    <!-- Logo -->
    <a href="{{asset('')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>L</b>MS</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Library</b> Management</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('')}}admin/user.png" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{str_limit(ucfirst($user->name), $limit = 10, $end = '...')}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{asset('')}}admin/user.png" class="img-circle" alt="User Image">

                            <p>
                                {{ucfirst($user->name)}} - {{ucfirst(str_replace('-',' ',$user->role))}}
                                {{--<small>Member since Nov. 2012</small>--}}
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">

                            <div class="pull-right">
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <btn class="btn btn-default btn-flat">Logout</btn>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>