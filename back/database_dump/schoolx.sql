-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 02, 2019 at 08:45 AM
-- Server version: 5.7.23
-- PHP Version: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `schoolx`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

DROP TABLE IF EXISTS `auth`;
CREATE TABLE IF NOT EXISTS `auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `authdate` varchar(50) NOT NULL,
  `authkey` varchar(255) NOT NULL,
  `expdate` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
CREATE TABLE IF NOT EXISTS `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) NOT NULL,
  `qualification` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `imageurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `firstname`, `lastname`, `qualification`, `description`, `imageurl`, `created_at`, `updated_at`) VALUES
(1, 'Mohamed', 'Jazeer', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chapter`
--

DROP TABLE IF EXISTS `chapter`;
CREATE TABLE IF NOT EXISTS `chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chapter`
--

INSERT INTO `chapter` (`id`, `subject_id`, `name`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '1 நுண்ணங்கிகளின் முக்கியத்துவம்', 1, '1', NULL, NULL),
(2, 1, '2 விலங்குப் பாகுபாடு', 2, '1', NULL, NULL),
(3, 1, '3 தாவரப் பகுதிகள பல்வகைமையும் அவற்றின் தொழில்', 3, '1', NULL, NULL),
(4, 1, '4 சடப்பொருளின் இயல்புகள்', 4, '1', NULL, NULL),
(5, 1, '5 ஒலி', 5, '1', NULL, NULL),
(6, 1, '6 காந்தம்', 6, '1', NULL, NULL),
(7, 1, '7 ஓட்ட மின்னியல் தொடர்பான அளவீடுகள்', 7, '1', NULL, NULL),
(8, 1, '8 சடப்பொருளில் ஏற்படும் மாற்றங்கள்', 8, '1', NULL, NULL),
(9, 1, '9 மனிதனின் உடற் தொகுதிகள்', 9, '1', NULL, NULL),
(10, 1, '10 மின்னியல்', 10, '1', NULL, NULL),
(11, 1, '11 தாவரங்களின் பிரதான உயிர் செயன்முறைகள்', 11, '1', NULL, NULL),
(12, 1, '12 அங்கிகளின் வாழ்க்கைச்சக்கரம்', 12, '1', NULL, NULL),
(13, 1, '13 நற்காப்பு', 13, '1', NULL, NULL),
(14, 1, '14 ஞாயிற்றுத் தொகுதி', 14, '1', NULL, NULL),
(15, 1, '15 இயற்கை அனர்த்தங்கள்', 15, '1', NULL, NULL),
(16, 3, '1 நுண்ணங்கிகளின் முக்கியத்துவம்', 1, '1', NULL, NULL),
(17, 3, '2 கண்ணும் காதும்', 2, '1', NULL, NULL),
(18, 3, '3 சடப்பொருட்களின் தன்மையும் அவற்றின் இயல்புகளும்', 3, '1', NULL, NULL),
(19, 3, '4 விசையும் அதனுடன் தொடர்புடைய அடிப்படை எண்ணக்கருக்', 4, '1', NULL, NULL),
(20, 3, '5 தின்மங்களினால் ஏற்படுத்தப்படும் அமுக்கம்', 5, '1', NULL, NULL),
(21, 3, '6 மனிதனின் குருதிச் சுற்றோட்டத்தொகு', 6, '1', NULL, NULL),
(22, 3, '7 தாவர வளர்ச்சிப் பதார்த்தங்கள்', 7, '1', NULL, NULL),
(23, 3, '8 அங்கிகளின் தாங்கும் இயல்பும் அசைவும்', 8, '1', NULL, NULL),
(24, 3, '9 அங்கிகளில் நிகழும் கூர்ப்பு செயன்முறை', 9, '1', NULL, NULL),
(25, 3, '10 மின்பகுப்பு', 10, '1', NULL, NULL),
(26, 3, '11 அடர்த்தி', 11, '1', NULL, NULL),
(27, 3, '12 உயிர்ப்பல்வகைமை', 12, '1', NULL, NULL),
(28, 3, '13 இயற்கைச் சூழலும் பசுமை எண்ணக்கருவும்', 13, '1', NULL, NULL),
(29, 3, '14 அலைத்தெறிப்பும் முறிவும்', 14, '1', NULL, NULL),
(30, 3, '15 எளிய பொறிகள்', 15, '1', NULL, NULL),
(31, 3, '16 நனோ தொழில் நுட்பமும் அதன் பிரயோகமும்', 16, '1', NULL, NULL),
(32, 3, '17 மின்னல் தாக்கம்', 17, '1', NULL, NULL),
(33, 3, '18 இயற்கை அனர்த்தங்கள்', 18, '1', NULL, NULL),
(34, 3, '19 இயற்கை வளங்களைப் பேண்தகு  முறையில் பயன்படுத்துத', 19, '1', NULL, NULL),
(35, 7, '1 தாவர இழையங்களும் விலங்கு இழையங்களும்', 1, '1', NULL, NULL),
(36, 7, '2 ஒளித்தொகுப்பு', 2, '1', NULL, NULL),
(37, 7, '3 கலவை', 3, '1', NULL, NULL),
(38, 7, '4 அலைகளும் அவற்றின் பயன்பாடுகளும்', 4, '1', NULL, NULL),
(39, 7, '5 கேத்திர கணிதம் ஒளியியல்', 5, '1', NULL, NULL),
(40, 7, '6 மனித உடற்செயன்முறைகள்', 6, '1', NULL, NULL),
(41, 7, '7 அமிலம், மூலம், உப்பு', 7, '1', NULL, NULL),
(42, 7, '8 இரசாயன தாக்கங்களின் வெப்ப விளைவு', 8, '1', NULL, NULL),
(43, 7, '9 வெப்பம்', 9, '1', NULL, NULL),
(44, 7, '10 மின்னுபகரணங்களின் வலுவும் சக்தியும்', 10, '1', NULL, NULL),
(45, 7, '11 இலத்திரனியல்', 11, '1', NULL, NULL),
(46, 7, '12 மின்னிரசாயனம்', 12, '1', NULL, NULL),
(47, 7, '13 மின்காந்தவியலும் தூண்டலும்', 13, '1', NULL, NULL),
(48, 7, '14 ஐநரோக்காபன்களும் அவற்றின் பெறுதிகளும்', 14, '1', NULL, NULL),
(49, 7, '15 உயிர்க்கோளம்', 15, '1', NULL, NULL),
(50, 5, '1 உயிரின இரசாயன அடிப்படை', 1, '1', NULL, NULL),
(51, 5, '2 நேர்கோட்டு இயக்கம்', 2, '1', NULL, NULL),
(52, 5, '3 சடப்பொருள்களின் கட்டமைப்பு', 3, '1', NULL, NULL),
(53, 5, '4 நியூற்றனின் இயக்க விதிகள்', 4, '1', NULL, NULL),
(54, 5, '5 உராய்வு', 5, '1', NULL, NULL),
(55, 5, '6 தாவரக்கலங்களினதும் விலங்குக்கலங்களினதும் கட்டமைப', 6, '1', NULL, NULL),
(56, 5, '7 மூலகங்களினதும் சேர்வைகளினதும் அளவறிதல்', 7, '1', NULL, NULL),
(57, 5, '8 அங்கிகளின் சிறப்பியல்புகள்', 8, '1', NULL, NULL),
(58, 5, '9 விளையுள் விசை', 9, '1', NULL, NULL),
(59, 5, '10 இரசாயனப் பிணைப்புகள்', 10, '1', NULL, NULL),
(60, 5, '11 விசையின் திரும்பல் விளைவு', 11, '1', NULL, NULL),
(61, 5, '12 விசைகளின் சமநிலை', 12, '1', NULL, NULL),
(62, 5, '13 உயிர்க்கோளம்', 13, '1', NULL, NULL),
(63, 5, '14 உயிரின் தொடர்ச்சி', 14, '1', NULL, NULL),
(64, 5, '15 நீர்நிலையில் அமுக்கமும் அதன் பிரயோகங்களும்', 15, '1', NULL, NULL),
(65, 5, '16 சடப் பொருள்களில் ஏற்படும் மாற்றம்', 16, '1', NULL, NULL),
(66, 5, '17 தாக்க வீதம்', 17, '1', NULL, NULL),
(67, 5, '18 வேலை, சக்தி, வலு', 18, '1', NULL, NULL),
(68, 5, '19 ஓட்ட மின்னியல்', 19, '1', NULL, NULL),
(69, 5, '20 தலைமுறை உரிமை', 20, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

DROP TABLE IF EXISTS `grade`;
CREATE TABLE IF NOT EXISTS `grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `name`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'தரம் 08', 1, '1', NULL, NULL),
(2, 'தரம் 09', 2, '1', NULL, NULL),
(3, 'தரம் 10', 3, '1', NULL, NULL),
(4, 'தரம் 11', 4, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

DROP TABLE IF EXISTS `material`;
CREATE TABLE IF NOT EXISTS `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `videolink` text,
  `heading` text,
  `source` varchar(50) DEFAULT NULL,
  `status` enum('Y','N') DEFAULT NULL,
  `image` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id`, `module_id`, `author_id`, `videolink`, `heading`, `source`, `status`, `image`, `created_at`, `updated_at`) VALUES
(6, 1, 1, 'https://www.youtube.com/embed/OjwHCpdlNRM', '4.1.0 அறிமுகம்', 'youtube', 'Y', '', NULL, NULL),
(7, 2, 1, 'https://www.youtube.com/watch?v=FPhetlu3f2g', 'Another Video', 'youtube', 'Y', 'https://i.imgur.com/fnbTGAL.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(6, '2014_10_12_000000_create_users_table', 2),
(7, '2014_10_12_100000_create_password_resets_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chapter_id` int(11) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `order` int(11) NOT NULL,
  `shortname` varchar(150) DEFAULT NULL,
  `status` enum('0','1') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `chapter_id`, `name`, `order`, `shortname`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, '4.1 சடப்பொருட்களின் தொடர்ச்சியற்ற தன்மை (4.1.0 அறிமுகம்)', 1, 'Module SN 1', '1', NULL, NULL),
(2, 4, '4.1 சடப்பொருட்களின் தொடர்ச்சியற்ற தன்மை (4.1.1 சக்தியும் சடப்பொருளும்)', 2, 'Module SN 2', '1', NULL, NULL),
(3, 4, '4.1 சடப்பொருட்களின் தொடர்ச்சியற்ற தன்மை (4.1.2 துணிக்கை இயல்பின் அடிப்படையில் சடப்பொருளின் பௌதிக இயல்புகள்) ', 3, 'Module SN 3', '0', NULL, NULL),
(5, 4, '4.2 சடப்பொருளின் பௌதிக இயல்புகளின் பிரயோகங்கள் (4.2.1 மூலகமும் சேர்வையும்)', 5, 'Module SN 5', '0', NULL, NULL),
(6, 4, '4.2 சடப்பொருளின் பௌதிக இயல்புகளின் பிரயோகங்கள் (4.2.2 சடப்பொருளின் இயல்புகளும் பயன்படும்)', 6, 'Module SN 6', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `modulerating`
--

DROP TABLE IF EXISTS `modulerating`;
CREATE TABLE IF NOT EXISTS `modulerating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating_id` int(11) NOT NULL,
  `flag` char(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nonregisteredusers`
--

DROP TABLE IF EXISTS `nonregisteredusers`;
CREATE TABLE IF NOT EXISTS `nonregisteredusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device` varchar(255) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `sessionid` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('29bbb46bad2a35c04108962dbfdc27466718cb96a76fba00070159f24cff7bbb81ee71dbd7c79c2d', 1, 1, 'TutsForWeb', '[]', 0, '2019-01-02 01:33:05', '2019-01-02 01:33:05', '2020-01-02 07:03:05'),
('523664856cb5f1187358ff42ddbba3a29b882b687ebd9732266d4eace02e46ecfbb3f9a8b4486c9f', 1, 1, 'TutsForWeb', '[]', 0, '2019-01-02 01:35:47', '2019-01-02 01:35:47', '2020-01-02 07:05:47'),
('2374e89507928297550793203c0ac5c80b81bcbed76ea7f9aa58a7030745d244d832eb7e9aaa2f7b', 2, 1, 'TutsForWeb', '[]', 0, '2019-01-02 01:36:41', '2019-01-02 01:36:41', '2020-01-02 07:06:41'),
('7aa6484d195db48f8229668c99e05da6a76a047645078dfe3d479b22c93f62b2f82c5045fa7c65ad', 1, 1, 'SchoolX', '[]', 0, '2019-01-02 01:49:09', '2019-01-02 01:49:09', '2020-01-02 07:19:09'),
('6c2a60032dcee8ccf4063b3ef6c9b592a4499aa0188af7440683bbc05d6a1b77fbc31c538d3b8cbc', 3, 1, 'SchoolX', '[]', 0, '2019-01-02 02:41:14', '2019-01-02 02:41:14', '2020-01-02 08:11:14'),
('25f2e8d930eddb070b109464665e5ed2546b674c565fe0c2443f7435aaaff42057a361438c154aad', 4, 1, 'SchoolX', '[]', 0, '2019-01-02 02:43:34', '2019-01-02 02:43:34', '2020-01-02 08:13:34'),
('a2a35537c4a7b82a0475290237b60986ebb4e9258f350fa3ca3c1a881929132931950ecd6dd59aac', 5, 1, 'SchoolX', '[]', 0, '2019-01-02 02:59:52', '2019-01-02 02:59:52', '2020-01-02 08:29:52'),
('c1d5b602c17906461e8dddc36bfb2e2a4e439dba2f7c39e89bc92ad5c269fc89e1862e3fe0fdc316', 7, 1, 'SchoolX', '[]', 0, '2019-01-02 03:05:46', '2019-01-02 03:05:46', '2020-01-02 08:35:46'),
('4cedf0e5b21d80eed676531f09213e2ae0ef6719a1065ebd36573004d0ca83ce62a3c4f54328e10d', 8, 1, 'SchoolX', '[]', 0, '2019-01-02 03:06:25', '2019-01-02 03:06:25', '2020-01-02 08:36:25');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '0xniUR4QWxrV0Gbvnf7PQCNfklY4HYigs81Z0Jis', 'http://localhost', 1, 0, 0, '2019-01-02 01:06:12', '2019-01-02 01:06:12'),
(2, NULL, 'Laravel Password Grant Client', '9OJoYErHPRbO5m2iZFpUr5I8nZv9YRcZZLprdHXg', 'http://localhost', 0, 1, 0, '2019-01-02 01:06:12', '2019-01-02 01:06:12');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-01-02 01:06:12', '2019-01-02 01:06:12');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
CREATE TABLE IF NOT EXISTS `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ratingname` varchar(255) NOT NULL,
  `shortcode` varchar(255) NOT NULL,
  `score` float NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registeredusers`
--

DROP TABLE IF EXISTS `registeredusers`;
CREATE TABLE IF NOT EXISTS `registeredusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device` varchar(255) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `sessionid` varchar(255) NOT NULL,
  `action` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
CREATE TABLE IF NOT EXISTS `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `grade_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'விஞ்ஞானம்', '1', NULL, NULL),
(2, 1, 'கணிதம்', '1', NULL, NULL),
(3, 2, 'விஞ்ஞானம்', '1', NULL, NULL),
(4, 2, 'கணிதம்', '1', NULL, NULL),
(5, 3, 'விஞ்ஞானம்', '1', NULL, NULL),
(6, 3, 'கணிதம்', '1', NULL, NULL),
(7, 4, 'விஞ்ஞானம்', '1', NULL, NULL),
(8, 4, 'கணிதம்', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `mobile` int(10) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `school` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'madawa', 'madawa@c.com', NULL, '$2y$10$A19NcopAYFeMBszmuKPIVebSzO29XmfST09h0CyZ/Dc4JQHIWP.va', NULL, '2019-01-02 01:33:05', '2019-01-02 01:33:05'),
(2, 'pramod', 'pramod@gmail.com', NULL, '$2y$10$67vBmMktdrGQX3rM1Ydxs.MoW5Z5FMQQMxHZztxbGTiqrdBNXucRS', NULL, '2019-01-02 01:36:40', '2019-01-02 01:36:40'),
(8, 'desh', 'desh@gmail.com', '0717717711', '$2y$10$aOS8WnieM0kEyOivAPY4g.pQeAQuOudJz4Hq2EzYIDNOzSkcUztz6', NULL, '2019-01-02 03:06:25', '2019-01-02 03:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `videorating`
--

DROP TABLE IF EXISTS `videorating`;
CREATE TABLE IF NOT EXISTS `videorating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating_id` int(11) NOT NULL,
  `flag` char(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
